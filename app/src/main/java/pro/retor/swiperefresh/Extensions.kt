package pro.retor.swiperefresh

import android.view.View


internal fun HashMap<View, EasyPullLayout.ChildViewAttr>.getByType(type: Int?): View? {
    for ((key) in this)
        if ((key.layoutParams as EasyPullLayout.LayoutParams).type == type)
            return key
    return null
}