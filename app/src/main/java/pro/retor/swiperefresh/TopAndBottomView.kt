package pro.retor.swiperefresh

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.view_top_and_bottom.view.*

class TopAndBottomView : LinearLayout {

    companion object {
        const val ANIMATION_DURATION = 1500L
    }

    private var toRotationReady = 180f
    private var toRotationIdle = 0f
    private var idleText = ""
    private var refreshingText = ""

    private var backResource = -1
    private var textColor = -1

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(getContext()).inflate(R.layout.view_top_and_bottom, this, true)
        context?.theme?.obtainStyledAttributes(attrs, R.styleable.TopAndBottomView, defStyleAttr, 0).let {
            idleText = it?.getString(R.styleable.TopAndBottomView_idleText) ?: ""
            refreshingText = it?.getString(R.styleable.TopAndBottomView_refreshingText) ?: ""
            backResource = it?.getResourceId(R.styleable.TopAndBottomView_backgroundResource, -1)
                    ?: -1
            textColor = it?.getResourceId(R.styleable.TopAndBottomView_fontColor, -1) ?: -1
        }
    }

    fun ready(fraction: Float) {
        if (backResource != -1) {
            backgroundLayout?.setBackgroundResource(backResource)
            startIcon?.setBackgroundResource(backResource)
        }
        if (textColor != -1)
            startTitle?.setTextColor(ContextCompat.getColor(context, textColor))
        progressIcon?.animation?.cancel()
        startTitle?.visibility = View.VISIBLE
        startIcon?.visibility = View.VISIBLE
        progressTitle?.visibility = View.GONE
        progressIcon?.apply {
            visibility = View.GONE
            invalidate()
        }
        startTitle?.setText(idleText)
        startIcon?.setProgress(fraction * 100)
    }

    fun triggered() {
        if (backResource != -1)
            backgroundLayout?.setBackgroundResource(backResource)
        if (textColor != -1)
            progressTitle?.setTextColor(ContextCompat.getColor(context, textColor))
        startTitle?.visibility = View.GONE
        startIcon?.visibility = View.GONE
        progressTitle?.visibility = View.VISIBLE
        progressTitle?.setText(refreshingText)
        progressIcon?.apply {
            animation?.cancel()
            visibility = View.VISIBLE
            requestLayout()
            startAnimation(RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f).apply {
                repeatMode = Animation.RESTART
                duration = ANIMATION_DURATION
                repeatCount = Animation.INFINITE
            })
        }
    }
}